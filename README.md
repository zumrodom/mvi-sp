****The length of the synthetic pathway of the compounds****

The aim of the thesis is to explore the complexity of molecules based on descriptors and structure and subsequent prediction using Neural network from Kears based on own data.

*Data for this work MVI_data.csv are available here: https://1drv.ms/u/s!AlERDoCAjVnnxSRxgfvLrZnHjzXl?e=JfT8bv* 

*Code:* MVI-prediction_synthetic_pathway.ipynb

*Milestone:* Milestone.pdf

*Report:* report.pdf
